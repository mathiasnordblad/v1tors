//
//  FrogVC.swift
//  v1tors
//
//  Created by Mathias Nordblad on 2018-08-30.
//  Copyright © 2018 Mathias Nordblad. All rights reserved.
//

import UIKit

class FrogVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    var Night = 0

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func NightMode(_ sender: Any) {
        if Night == 0 {
            Night = 1
            print("Night on")
            BackgroundText.textColor = UIColor.white
            BackgroundText.backgroundColor = UIColor.black
            Backdrop.backgroundColor = UIColor.black
            SwitchLabel.textColor = UIColor.orange
            SwitchLabel.backgroundColor = UIColor.black
        }
        else {
            Night = 0
            BackgroundText.textColor = UIColor.black
            BackgroundText.backgroundColor = UIColor.white
            Backdrop.backgroundColor = UIColor.white
            SwitchLabel.textColor = UIColor.black
            SwitchLabel.backgroundColor = UIColor.white
            print("Night off")
        }
    }
    @IBOutlet weak var BackgroundText: UILabel!
    @IBOutlet var Backdrop: UIView!
    @IBOutlet weak var SwitchLabel: UILabel!
    @IBOutlet weak var NextButton: UIButton!
}
